import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { SubscribeService } from "../../services/subscribe.service"

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

   subscribeForm = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email])
  });

  constructor( private subscribeService: SubscribeService) { }

  ngOnInit() {
  }

  subscribeEmail() {
    console.log("email from data", this.subscribeForm.value)
    this.subscribeService.send(this.subscribeForm.value).subscribe((data: any) => {
     console.log("inside errror")
    }, (error: any) => {
      if (error.status == 404) {
         console.log("error", error)
        if (error.statusText == 'Not Found') {
          console.log("Not Found")
        }

      }

    })
  }
  // get email() {
  //   return this.contactForm.get("email");
  // }

}
