import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SubscribeService {
  url: any;
  httpOptions: any;

  constructor(private http: HttpClient) {

    this.url = 'https://example.com/api/subscribe';


    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      })
    };
  }

  send(data: any) {
    return this.http.post(this.url,data,this.httpOptions);
  }
}
